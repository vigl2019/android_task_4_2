package com.example.task_4_2;

/*

2*. Расположить на экране два контейнера на каждом по три текстовых поля для ввода цифр, на первом контейнере есть еще и три кнопки.
После ввода цифр в десятичном формате и нажатии соответствующей кнопки в первом контейнере -
во втором контейнере соответствующее поле заполняется тем же числом,
но в другой системе счисления (двоичной, восьмеричной и шестнадцатеричной соответственно).

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Calculator;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.first_number)
    EditText first_number;

    @BindView(R.id.second_number)
    EditText second_number;

    @BindView(R.id.third_number)
    EditText third_number;

    @BindView(R.id.first_number_button)
    Button first_number_button;

    @BindView(R.id.second_number_button)
    Button second_number_button;

    @BindView(R.id.third_number_button)
    Button third_number_button;

    @BindView(R.id.first_number_view)
    TextView first_number_view;

    @BindView(R.id.second_number_view)
    TextView second_number_view;

    @BindView(R.id.third_number_view)
    TextView third_number_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    //================================================================================//

    @OnClick({R.id.first_number_button, R.id.second_number_button, R.id.third_number_button})
    public void OnButtonClick(View view) {

        int first_number_button_id = first_number_button.getId();
        int second_number_button_id = second_number_button.getId();
        int third_number_button_id = third_number_button.getId();

        int buttonId = view.getId();

        if (buttonId == first_number_button_id) {

            int firstNumberInt = Integer.parseInt(first_number.getText().toString().trim());

            String firstNumber = Calculator.getBinaryNumber(firstNumberInt);
            first_number_view.setText(firstNumber);

            String secondNumber = Calculator.getOctalNumber(firstNumberInt);
            second_number_view.setText(secondNumber);

            String thirdNumber = Calculator.getHexaDecimalNumber(firstNumberInt);
            third_number_view.setText(thirdNumber);

        }

        if (buttonId == second_number_button_id) {

            int secondNumberInt = Integer.parseInt(second_number.getText().toString().trim());

            String firstNumber = Calculator.getBinaryNumber(secondNumberInt);
            first_number_view.setText(firstNumber);

            String secondNumber = Calculator.getOctalNumber(secondNumberInt);
            second_number_view.setText(secondNumber);

            String thirdNumber = Calculator.getHexaDecimalNumber(secondNumberInt);
            third_number_view.setText(thirdNumber);

        }

        if (buttonId == third_number_button_id) {

            int thirdNumberInt = Integer.parseInt(third_number.getText().toString().trim());

            String firstNumber = Calculator.getBinaryNumber(thirdNumberInt);
            first_number_view.setText(firstNumber);

            String secondNumber = Calculator.getOctalNumber(thirdNumberInt);
            second_number_view.setText(secondNumber);

            String thirdNumber = Calculator.getHexaDecimalNumber(thirdNumberInt);
            third_number_view.setText(thirdNumber);

        }
    }
}

